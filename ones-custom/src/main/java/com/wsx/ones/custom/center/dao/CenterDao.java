package com.wsx.ones.custom.center.dao;

import com.wsx.ones.custom.center.model.CustomNotice;

import java.util.List;
import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/4.
 */
public interface CenterDao {

    int saveNotice(CustomNotice notice);

    List<Map<String,Object>> getFiveNotices();
}
