package com.wsx.ones.language.common;

import com.wsx.ones.finalstr.common.BaiduTransUtil;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Map;

/**
 * Created by wangshuaixin on 17/1/5.
 */
public class BaiduParamUtil {

    public static String getSign(Map<String, String> param) {
        //对appId+源文+随机数+token计算md5值
        StringBuffer buffer = new StringBuffer();
        buffer.append(param.get(BaiduTransUtil.TRANS_APPID))
                .append(param.get(BaiduTransUtil.SOURCE_CONTENT))
                .append(param.get(BaiduTransUtil.TRANS_SALT))
                .append(BaiduConfig.getToken());

        return DigestUtils.md5Hex(buffer.toString());
    }
}
