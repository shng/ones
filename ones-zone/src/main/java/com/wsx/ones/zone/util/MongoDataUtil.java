package com.wsx.ones.zone.util;

import com.wsx.ones.common.util.DateUtil;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by wangshuaixin on 16/12/30.
 */
public class MongoDataUtil {

    /**
     * mongodb查询返回的数据是LinkedHashMap中String Object的，Object可能也是map，我们只是需要value值
     * @param origin
     * @return
     */
    public static Map<String, String> transFromObject(Map<String, Object> origin) {
        Map<String, String> map = new HashMap<String, String>();
        if (null == origin || origin.isEmpty()) {
            return map;
        }

        Iterator<String> iterator = origin.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Object value = origin.get(key);
            if (value instanceof ObjectId) {
                map.put(key, ((ObjectId)value).toHexString());
            } else if (value instanceof String) {
                map.put(key, String.valueOf(value));
            } else if (value instanceof Integer || value instanceof Long) {
                map.put(key, String.valueOf(value));
            } else if (value instanceof Date) {
                map.put(key, DateUtil.getStringDate((Date) value));
            } else {
                map.put(key, value.toString());
            }
        }
        return map;
    }
}
