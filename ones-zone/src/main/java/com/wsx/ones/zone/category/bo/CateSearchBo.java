package com.wsx.ones.zone.category.bo;

import com.wsx.ones.web.bo.PageBo;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/30.
 */
public class CateSearchBo extends PageBo {

    private static final long serialVersionUID = -835570914969915279L;

    private String name;
    private String atime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAtime() {
        return atime;
    }

    public void setAtime(String atime) {
        this.atime = atime;
    }
}
