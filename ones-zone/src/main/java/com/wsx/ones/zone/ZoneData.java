package com.wsx.ones.zone;

import com.wsx.ones.web.model.OutputData;

/**
 * Created by wangshuaixin on 16/12/23.
 */
public class ZoneData extends OutputData {

    private static final long serialVersionUID = 5737803032429598548L;

    private String _id;

    public ZoneData() {
        super();
    }

    public ZoneData (int code, int status) {
        super(code, status);
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
