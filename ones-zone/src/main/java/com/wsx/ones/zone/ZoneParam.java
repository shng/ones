package com.wsx.ones.zone;

import com.wsx.ones.web.model.InputParam;

/**
 * Created by wangshuaixin on 16/12/23.
 */
public class ZoneParam extends InputParam {

    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
