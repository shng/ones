package com.wsx.ones.zone.category.model;

import com.wsx.ones.core.model.BaseBean;

import java.util.Date;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class CateSearch extends BaseBean {

    private String name;
    private String atime;

    private Integer pageNo;
    private Integer pageSize;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAtime() {
        return atime;
    }

    public void setAtime(String atime) {
        this.atime = atime;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
