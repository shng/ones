package com.wsx.ones.zone.content;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wsx.ones.web.model.OutputData;
import com.wsx.ones.web.util.OperationEnum;
import com.wsx.ones.zone.ZoneParam;
import com.wsx.ones.zone.content.data.ContentData;
import com.wsx.ones.zone.content.param.ContentParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wsx.ones.zone.ZoneBaseController;
import com.wsx.ones.zone.content.model.mongo.ZoneContent;
import com.wsx.ones.zone.content.service.ZoneContentService;


/**
 * 
 * @author wangshuaixin
 *
 */
@RestController
@Scope("prototype")
public class ZoneContentController extends ZoneBaseController {
	
	@Autowired
	private ZoneContentService zoneContentService;

	@RequestMapping(
			value = "/content/publish",
			method = {RequestMethod.POST}
	)
	public @ResponseBody OutputData publishContent(ContentParam contentParam, HttpServletRequest request, HttpServletResponse response) {
		
		if (!checkZoneContent(contentParam, OperationEnum.ADD)) {
			throw new RuntimeException("null");
		}
		
		//boolean isSave = zoneContentService.saveZoneContent(zoneContent);
		

		
		//return "WEB-INF/jsp/zoneContent/zone_content";
		return new ContentData();
	}


}
