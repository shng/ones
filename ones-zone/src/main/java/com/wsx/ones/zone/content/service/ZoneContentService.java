package com.wsx.ones.zone.content.service;

import com.wsx.ones.zone.content.model.mongo.ZoneContent;

public interface ZoneContentService {

	public boolean saveZoneContent(ZoneContent zoneContent);
}
