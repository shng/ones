package com.wsx.ones.zone.comment.dao;

import com.wsx.ones.zone.comment.model.mongo.ZoneComment;

/**
 * Created by wangshuaixin on 16/12/11.
 */
public interface ZoneCommentDao {

    void saveComment(ZoneComment zoneComment);
}
