package com.wsx.ones.zone;


import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 16/12/29.
 */
public class ZoneVo extends BaseVo {

    private static final long serialVersionUID = -8264465081001745172L;

    public ZoneVo () {
        super();
    }

    public ZoneVo (ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
