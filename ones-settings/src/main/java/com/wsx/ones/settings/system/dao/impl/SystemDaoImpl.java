package com.wsx.ones.settings.system.dao.impl;

import com.wsx.ones.settings.system.dao.SystemDao;
import com.wsx.ones.settings.system.mapper.SystemMapper;
import com.wsx.ones.settings.system.model.System;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by wangshuaixin on 16/12/11.
 */
@Repository
public class SystemDaoImpl implements SystemDao {

    @Resource
    private SystemMapper systemMapper;

    public int saveTest(System system) {
        return systemMapper.saveTest(system);
    }
}
