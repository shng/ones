package com.wsx.ones.settings.system.model;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/11.
 */
public class System extends BaseBean {

    private static final long serialVersionUID = 7750623148752367118L;

    private Integer id;
    private String test;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
