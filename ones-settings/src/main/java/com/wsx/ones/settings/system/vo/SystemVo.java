package com.wsx.ones.settings.system.vo;

import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.vo.BaseVo;

/**
 * Created by wangshuaixin on 17/1/8.
 */
public class SystemVo extends BaseVo {

    private static final long serialVersionUID = -209722168880162993L;

    public SystemVo() {
        super();
    }
    public SystemVo(ReturnStatus returnStatus) {
        super(returnStatus);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
