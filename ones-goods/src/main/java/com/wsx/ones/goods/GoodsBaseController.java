package com.wsx.ones.goods;

import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/Goods")
public abstract class GoodsBaseController extends BaseController implements WebController {

    protected boolean checkGoods(GoodsBaseBean bean) {
        if (null == bean) {
            return false;
        }

        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
