package com.wsx.ones.goods;

import com.wsx.ones.core.model.BaseBean;

/**
 * Created by wangshuaixin on 16/12/9.
 */
public class GoodsBaseBean extends BaseBean {

    private static final long serialVersionUID = 7446854942423049507L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
