package com.wsx.ones.vip.home;

import com.wsx.ones.vip.VipBaseController;
import com.wsx.ones.web.model.OutputData;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/17.
 */
@RestController
@Scope("prototype")
public class VipHomeController extends VipBaseController {


    public OutputData getHomePage() {

        return new OutputData();
    }
}
