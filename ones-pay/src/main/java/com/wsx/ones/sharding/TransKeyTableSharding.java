package com.wsx.ones.sharding;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;
import com.wsx.ones.finalstr.common.CommonFinalUtil;

import java.util.Collection;

/**
 * 比如数据
 *          用户             交易       序列           组合健                存储位置
 *          1               10021      999        1-10021-999          trans_001_1
 *          1               20001      1002       1-20001-1002         trans_001_2
 *          2               20002       98        2-20002-98           trans_002_1
 *
 *   交易编号是大序列，一个业务线只有一个唯一的编号
 *   序列是每个用户%100所属的主业务表后生成的业务主表的序列
 * 0 ~ 3000000 table_1    3000000 ~ 6000000 table_2  ……的算法实现，可以动态的扩展数据库
 * 前期可以考虑使用100张表来分表，后期再扩展
 * Created by wangshuaixin on 17/1/6.
 */
public class TransKeyTableSharding implements SingleKeyTableShardingAlgorithm<Integer> {

    @Override
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {

        for (String table : availableTargetNames) {
            if (shardingValue.getValue()
                    == Integer.valueOf(table.substring(table.lastIndexOf(CommonFinalUtil.DOWN_SPIT) + 1))) {
                return table;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }

    @Override
    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        return null;
    }
}
