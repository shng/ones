package com.wsx.ones.pay.account.service.impl;

import com.wsx.ones.pay.account.bo.AccountBo;
import com.wsx.ones.pay.account.dao.AccountDao;
import com.wsx.ones.pay.account.model.PayTest;
import com.wsx.ones.pay.account.service.AccountService;
import com.wsx.ones.pay.account.vo.AccountVo;
import com.wsx.ones.web.swap.ReturnStatus;
import com.wsx.ones.web.util.PojoCopy;
import com.wsx.ones.web.vo.BaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/14.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Transactional(propagation = Propagation.REQUIRED)
    public AccountVo savePay(AccountBo bo) {

        PayTest payTest = PojoCopy.copy(bo, PayTest.class);

        int count = accountDao.savePay(payTest);
        if (count <= 0) {
            return new AccountVo(ReturnStatus.ERROR_UNKNOW);
        }

        AccountVo vo = new AccountVo();
        vo.setId(payTest.getPid());

        return vo;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public AccountVo saveTest(AccountBo bo) {
        PayTest test = PojoCopy.copy(bo, PayTest.class);

        int count = accountDao.saveTest(test);
        if (count <= 0) {
            return new AccountVo(ReturnStatus.ERROR_UNKNOW);
        }

        AccountVo vo = new AccountVo();
        vo.setId(test.getPid());

        return vo;
    }
}
