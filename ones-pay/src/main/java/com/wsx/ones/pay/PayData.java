package com.wsx.ones.pay;

import com.wsx.ones.web.model.OutputData;

/**
 * Created by wangshuaixin on 16/12/25.
 */
public class PayData extends OutputData {

    private static final long serialVersionUID = -8648327533285598626L;

    public PayData () {
        super();
    }
    public PayData(int code, int status) {
        super(code, status);
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
