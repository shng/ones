package com.wsx.ones.pay.account.dao.impl;

import com.wsx.ones.pay.account.dao.AccountDao;
import com.wsx.ones.pay.account.mapper.AccountMapper;
import com.wsx.ones.pay.account.model.PayTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by wangshuaixin on 16/12/14.
 */
@Repository
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public int savePay(PayTest payTest) {
        return accountMapper.savePay(payTest);
    }

    @Override
    public int saveTest(PayTest test) {
        return accountMapper.saveTest(test);
    }
}
