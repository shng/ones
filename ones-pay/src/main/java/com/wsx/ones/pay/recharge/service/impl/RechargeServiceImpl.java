package com.wsx.ones.pay.recharge.service.impl;

import com.wsx.ones.pay.recharge.bo.RechargeBo;
import com.wsx.ones.pay.recharge.dao.RechargeDao;
import com.wsx.ones.pay.recharge.model.Recharge;
import com.wsx.ones.pay.recharge.service.RechargeService;
import com.wsx.ones.pay.recharge.vo.RechargeVo;
import com.wsx.ones.web.util.PojoCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@Service
@Transactional
public class RechargeServiceImpl implements RechargeService {

    @Autowired
    private RechargeDao rechargeDao;


    @Transactional(propagation = Propagation.REQUIRED)
    public RechargeVo rechargeAmount(RechargeBo bo) {

        Recharge recharge = PojoCopy.copy(bo, Recharge.class);

        rechargeDao.saveRecharge(recharge);
        return null;
    }
}
