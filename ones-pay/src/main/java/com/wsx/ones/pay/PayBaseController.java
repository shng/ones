package com.wsx.ones.pay;


import com.wsx.ones.web.controller.BaseController;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.BaseUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/8.
 */
@RestController
@RequestMapping("/Pay")
public class PayBaseController extends BaseController implements WebController {

    protected boolean checkPayParam(PayParam payParam) {
        return true;
    }

    @Override
    public BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache) {
        return null;
    }

    @Override
    public BaseUser getUserCache(String uid, boolean ifEhcache) {
        return null;
    }
}
