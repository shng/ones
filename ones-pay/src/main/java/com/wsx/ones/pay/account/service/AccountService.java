package com.wsx.ones.pay.account.service;

import com.wsx.ones.pay.account.bo.AccountBo;
import com.wsx.ones.pay.account.vo.AccountVo;

/**
 * Created by wangshuaixin on 16/12/14.
 */
public interface AccountService {

    AccountVo savePay(AccountBo bo);

    AccountVo saveTest(AccountBo bo);
}
