CREATE TABLE `ad_content` (
  `aid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `mtype` tinyint(4) NOT NULL DEFAULT '1',
  `aname` varchar(50) NOT NULL DEFAULT '',
  `alink` varchar(100) NOT NULL DEFAULT '',
  `acode` varchar(500) NOT NULL DEFAULT '',
  `stime` datetime NOT NULL,
  `etime` datetime NOT NULL,
  `aclick` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;