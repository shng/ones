package com.wsx.ones.ad.index.param;

import com.wsx.ones.ad.AdEnum;
import com.wsx.ones.ad.AdParam;

/**
 * Created by wangshuaixin on 16/12/25.
 */
public class IndexAdParam extends AdParam {

    private AdEnum adEnum;

    public AdEnum getAdEnum() {
        return adEnum;
    }

    public void setAdEnum(AdEnum adEnum) {
        this.adEnum = adEnum;
    }
}
