package com.wsx.ones.ad;

/**
 * Created by wangshuaixin on 17/1/3.
 */
public enum AdEnum {
    INDEX_HEAD,
    INDEX_CONTENT,
    INDEX_LEFT,
    INDEX_RIGHT,
    INDEX_BOTTOM
}
