package com.wsx.ones.ad.model;

import com.wsx.ones.ad.AdBaseBean;

import java.util.Date;

/**
 * Created by wangshuaixin on 17/1/3.
 */
public class AdContent extends AdBaseBean {

    private static final long serialVersionUID = -1187817763123795142L;

    private Long aid;//
    private Long pid;//
    private Integer mtype;//类型，主要是 0图片;1flash;2代码3文字
    private String aname;
    private String alink;//广告的链接地址
    private String acode;//广告链接的表现，<a>标签的内容

    private Date stime;
    private Date etime;

    private Integer aclick;

    public Long getAid() {
        return aid;
    }

    public void setAid(Long aid) {
        this.aid = aid;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Integer getMtype() {
        return mtype;
    }

    public void setMtype(Integer mtype) {
        this.mtype = mtype;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAlink() {
        return alink;
    }

    public void setAlink(String alink) {
        this.alink = alink;
    }

    public String getAcode() {
        return acode;
    }

    public void setAcode(String acode) {
        this.acode = acode;
    }

    public Date getStime() {
        return stime;
    }

    public void setStime(Date stime) {
        this.stime = stime;
    }

    public Date getEtime() {
        return etime;
    }

    public void setEtime(Date etime) {
        this.etime = etime;
    }

    public Integer getAclick() {
        return aclick;
    }

    public void setAclick(Integer aclick) {
        this.aclick = aclick;
    }
}
