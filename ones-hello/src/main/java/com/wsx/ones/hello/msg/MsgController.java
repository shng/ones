package com.wsx.ones.hello.msg;

import com.wsx.ones.hello.HelloBaseController;
import com.wsx.ones.hello.msg.model.Msg;
import com.wsx.ones.hello.msg.service.MsgService;
import com.wsx.ones.web.controller.WebController;
import com.wsx.ones.web.model.OutputData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by wangshuaixin on 16/12/10.
 */
@RestController
@Scope("prototype")
public class MsgController extends HelloBaseController {

    private static final Logger log = LoggerFactory.getLogger(MsgController.class);


    @Autowired
    private MsgService msgService;

    @RequestMapping(
            value = "/msg/save",
            method = {RequestMethod.POST}
    )
    public @ResponseBody OutputData saveMsg(Msg msg, HttpServletRequest request, HttpServletResponse response) {

        log.info("enter msg");

        if (!checkMsg(msg)) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }

        boolean isSave = msgService.saveMsg(msg);
        if (!isSave) {
            return new OutputData(WebController.CODE_SUCCESS, WebController.STATUS_ERROR_CHECKCODE);
        }
        return new OutputData();
    }


}
