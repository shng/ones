package com.wsx.ones.goods.rpc.model;

import java.io.Serializable;

/**
 * Created by wangshuaixin on 16/12/30.
 */
public class Goods implements Serializable {

    private static final long serialVersionUID = 5543897323156180477L;

    private String name;
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
