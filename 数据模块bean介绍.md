该文件主要对各个业务功能模块中三层关系中数据传输的说明，分两个代表来说明：
    注：入的意思，就是方法接受的参数类型；出的意思就是方法处理完返回的数据类型
1，各层间的传输采用相同的bean来做数据传递

    controller          (入)param              (出)data
                     param --> bean        bean --> data

    service             (入)bean               (出)bean


    dao                 (入)bean               (出)bean
    
    demo:
    public class TestController {
        //接收的参数是param类型的，返回的数据类型是data
        public Data exec(Param param,……) {
            //中间需要做数据转换，此时要求字段名称和类型必须一致
            Bean bean = Util.copy(param,Bean);



2，各层在数据传输时采用不同的结构来传输：

    controller            (入)param              (出)data
    --------------------------------------------------------
                      param --> bo             vo --> data
                                |              ^
                                V              |
    --------------------------------------------------------
    service                 (入)bo            (出)vo
                          bo --> bean        bean --> vo
                                  |           ^
                                  V           |
    --------------------------------------------------------
    dao                     (入)bean            (出)bean


第二种模式是我所比较喜欢，主要原因如下：
    1，controller层是对外提供接口服务的，暴露出去的内容是经过封装和处理的，和内部数据结构是隔离的，需要做到意义转变
    2，service是业务处理层，涉及到很多具体的业务逻辑，有时候一个业务涉及到多张表的操作，就需要将一个bo转换成不同的bean
       并且所有的异常处理几乎都在该层进行处理和包装，上传给controller的只是对应错误的状态
    3，如果可以的话，可能只需要在controller层增加数据处理即可
                            
