package com.wsx.ones.web.model;

import com.wsx.ones.web.BoVo;
import com.wsx.ones.web.controller.WebController;


/**
 * 主要设计为返回接口的数据，controller层返回的数据结构
 * Created by wangshuaixin on 16/12/16.
 */
public class OutputData implements BoVo {

    private static final long serialVersionUID = -1955638843764941825L;

    private int code = WebController.CODE_SUCCESS;
    private int status = WebController.STATUS_SUCCESS;

    public OutputData() {
        super();
    }
    public OutputData(int code, int status) {
        this.code = code;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
