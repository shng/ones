package com.wsx.ones.web.vo;

import com.wsx.ones.web.BoVo;
import com.wsx.ones.web.swap.ReturnStatus;


/**
 * Created by wangshuaixin on 16/12/19.
 */
public class BaseVo implements BoVo {

    private static final long serialVersionUID = 4737812172093938323L;

    private ReturnStatus returnStatus = ReturnStatus.SUCCESS;

    public BaseVo() {

    }

    public BaseVo(ReturnStatus returnStatus) {
        this.returnStatus = returnStatus;
    }

    public ReturnStatus getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(ReturnStatus returnStatus) {
        this.returnStatus = returnStatus;
    }
}
