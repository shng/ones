package com.wsx.ones.web.controller;

import com.wsx.ones.web.model.BaseUser;
import com.wsx.ones.web.model.PageParam;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by wangshuaixin on 16/12/15.
 */
@RestController
public abstract class BaseController {

    @ExceptionHandler
    public String exceptionHandler(Exception ex, Model model) {
        model.addAttribute("message", ex.getMessage());
        return "error";
    }

    /**
     * 验证用户token是否有效
     * @param token
     * @return
     */
    protected boolean checkToken(String token) {
        if (null == token) {
            return false;
        }
        if (token.length() <= 10) {
            return false;
        }
        return true;
    }

    /**
     * 分页查询的验证
     * @param pageParam
     * @return
     */
    protected boolean checkPageParam(PageParam pageParam) {
        if (!checkToken(pageParam.getToken())) {
            return false;
        }
        if (null == pageParam.getPageNo() || pageParam.getPageNo() <= 0) {
            pageParam.setPageNo(1);
        }
        if (null == pageParam.getPageSize() || pageParam.getPageSize() <= 0) {
            pageParam.setPageSize(10);
        }

        return true;
    }

    public abstract BaseUser setUserCache(BaseUser baseUser, boolean ifEhcache);

    public abstract BaseUser getUserCache(String uid, boolean ifEhcache);

}
