package com.wsx.ones.web.util;

import org.springframework.beans.BeanUtils;

/**
 * Created by wangshuaixin on 16/12/19.
 */
public class PojoCopy {

    /**
     * 对象之间的拷贝操作
     * @param from
     * @param to
     * @return
     */
    public static boolean copy(Object from, Object to) {
        try {
            BeanUtils.copyProperties(from, to);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 拷贝到对应的目标对象之间
     * @param from
     * @param to
     * @param <T>
     * @return
     */
    public static <T> T copy(Object from, Class<T> to) {
        T rtn = null;
        try {
            rtn = BeanUtils.instantiate(to);
            BeanUtils.copyProperties(from, rtn);
        } catch (Exception e) {
            return null;
        }
        return rtn;
    }
}
