package com.wsx.ones.sharding.user;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.google.common.collect.Range;
import com.wsx.ones.finalstr.common.CommonFinalUtil;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * Created by wangshuaixin on 16/12/7.
 */

public class UserTypeDatabaseSharding implements SingleKeyDatabaseShardingAlgorithm<Integer> {


    /**
     * 分库的操作，根据用户类型来分库，个人和企业进行不同的数据库存储
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        //循环遍历可用的数据源信息
        for (String database : availableTargetNames) {
            //系统只分为个人和企业，并且个人为1，企业为2，不可修改，数据源的设定也是_1,_2的设定
            if (database.endsWith(shardingValue.getValue() + CommonFinalUtil.STRING_EMPTY)) {
                return database;
            }
        }
        throw  new IllegalArgumentException();
    }

    /**
     * 分库的in操作时涉及到数据源
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        //定义返回数据源集合
        Collection<String> databases = new LinkedHashSet<String>(availableTargetNames.size());
        //获得where条件中in中的所有值
        for (Integer value : shardingValue.getValues()) {
            //获得所有的可用数据源
            for (String database : availableTargetNames) {
                //如果数据的数值和数据源的后缀匹配则添加到要查询的数据中
                if (database.endsWith(value + CommonFinalUtil.STRING_EMPTY)) {
                    databases.add(database);
                }
            }
        }

        return databases;
    }

    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Integer> shardingValue) {
        //定义返回数据源的集合
        Collection<String> databases = new LinkedHashSet<String>(availableTargetNames.size());
        //获得使用where条件中betwee中的开始结束对象
        Range<Integer> range = shardingValue.getValueRange();
        //对where中between中的返回开始遍历
        for (Integer value = range.lowerEndpoint(); value <= range.upperEndpoint(); value++) {
            for (String database : availableTargetNames) {
                //如果后缀相同则进行加入数据
                if (database.endsWith(value + CommonFinalUtil.STRING_EMPTY)) {
                    databases.add(database);
                }
            }
        }
        return databases;
    }
}
