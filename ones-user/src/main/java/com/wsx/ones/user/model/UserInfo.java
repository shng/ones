package com.wsx.ones.user.model;

import com.wsx.ones.user.UserBaseBean;

/**
 * Created by wangshuaixin on 16/12/19.
 * 用户核心表
 */
public class UserInfo extends UserBaseBean {

    private static final long serialVersionUID = 4511517787773413356L;

    //private Long uid;
    private String lname;
    private String nname;
    private String passwd;
    private String avatar;
    private String phone;
    private String email;
    private Integer status;

    //分库
    //private Integer utype;

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getNname() {
        return nname;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
