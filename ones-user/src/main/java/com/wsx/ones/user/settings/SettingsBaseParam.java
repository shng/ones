package com.wsx.ones.user.settings;

import com.wsx.ones.user.UserParam;

/**
 * Created by wangshuaixin on 16/12/28.
 */
public class SettingsBaseParam extends UserParam {

    private Long uid;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
