package com.wsx.ones.user.signup.dao;

import java.util.List;

import com.wsx.ones.user.model.UserBase;
import com.wsx.ones.user.model.UserExt1;
import com.wsx.ones.user.model.UserExt2;
import com.wsx.ones.user.model.UserInfo;
import com.wsx.ones.user.signup.model.User;
import com.wsx.ones.user.signup.model.WebUser;

public interface UserSignupDao {

	public boolean saveUser(User user);
	
	public List<User> findAllUsers();
	
	public List<User> findByUserByIds(List<Integer> userIds);

	public boolean saveWebUser(WebUser user);

	public WebUser getWebUserByID(String userId);

	public List<WebUser> findByWebUserByIds(List<String> ids);

	public List<User> getUsersByName(String name);

	public List<User> findUsersByName(String name);

    int saveUserInfo(UserInfo userInfo);

	int saveUserBase(UserBase userBase);

	int saveUserExt1(UserExt1 userExt1);

	int saveUserExt2(UserExt2 userExt2);
}
