package com.wsx.ones.user.signup.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import com.wsx.ones.user.model.UserBase;
import com.wsx.ones.user.model.UserExt1;
import com.wsx.ones.user.model.UserExt2;
import com.wsx.ones.user.model.UserInfo;
import com.wsx.ones.user.signup.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.wsx.ones.user.signup.dao.UserSignupDao;
import com.wsx.ones.user.signup.model.User;
import com.wsx.ones.user.signup.model.WebUser;


@Repository
public class UserSignupDaoImpl implements UserSignupDao {

	@Resource
	private UserMapper userMapper;
	
	@Resource
	private WebUserMapper webUserMapper;

	@Autowired
	private UserInfoMapper userInfoMapper;

	@Autowired
	private UserBaseMapper userBaseMapper;

	@Autowired
	private UserExt1Mapper userExt1Mapper;

	@Autowired
	private UserExt2Mapper userExt2Mapper;

	@Override
	public boolean saveUser(User user) {
		return userMapper.saveUser(user) > 0 ? true : false;
	}

	@Override
	public List<User> findAllUsers() {
		return userMapper.findAllUsers();
	}

	@Override
	public List<User> findByUserByIds(List<Integer> userIds) {
		return userMapper.findByUserIds(userIds);
	}
	
	@Override
	public boolean saveWebUser(WebUser user) {
		// TODO Auto-generated method stub
		return webUserMapper.saveWebUser(user) > 0 ? true : false;
	}
	
	@Override
	public WebUser getWebUserByID(String userId) {
		return webUserMapper.getWebUserByID(userId);
	}
	
	@Override
	public List<WebUser> findByWebUserByIds(List<String> ids) {
		return webUserMapper.findByWebUserIds(ids);
	}

	@Override
	public List<User> getUsersByName(String name) {
		return userMapper.getUsersByName(name);
		//return userMapper.getUsersBetween(10,50);
	}

	@Override
	public List<User> findUsersByName(String name) {
		return userMapper.findUsersByName(name);
	}



	@Override
	public int saveUserInfo(UserInfo userInfo) {
		return userInfoMapper.saveUserInfo(userInfo);
	}

	@Override
	public int saveUserBase(UserBase userBase) {
		return userBaseMapper.saveUserBase(userBase);
	}

	@Override
	public int saveUserExt1(UserExt1 userExt1) {
		return userExt1Mapper.saveUserExt1(userExt1);
	}

	@Override
	public int saveUserExt2(UserExt2 userExt2) {
		return userExt2Mapper.saveUserExt2(userExt2);
	}
}
