package com.wsx.ones.user.signup.mapper;

import com.wsx.ones.user.model.UserInfo;

/**
 * Created by wangshuaixin on 16/12/21.
 */
public interface UserInfoMapper {

    int saveUserInfo(UserInfo userInfo);
}
