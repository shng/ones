package com.wsx.ones.user.signup.mapper;

import com.wsx.ones.user.model.UserExt1;

/**
 * Created by wangshuaixin on 16/12/27.
 */
public interface UserExt1Mapper {

    int saveUserExt1(UserExt1 userExt1);
}
