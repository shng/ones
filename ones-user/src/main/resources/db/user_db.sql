/**

采用基于mysql的架构设计

 */

/**
该sql不能执行，需要将标明修改下，创建的是分表机制_0,_1等
 */
CREATE TABLE `user_info_{0……9}` (
  `uid` int(11) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `nname` varchar(100) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100),
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_base_{0……9}` (
  `uid` int(11) NOT NULL,
  `province` tinyint(4) DEFAULT NULL,
  `city` tinyint(4) DEFAULT NULL,
  `country` tinyint(4) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `rlname` varchar(100) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `nation` tinyint(4) DEFAULT NULL,
  `idtype` tinyint(4) DEFAULT NULL,
  `idnum` varchar(50) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `secdemail` varchar(100) DEFAULT NULL,
  `memo` varchar(200) DEFAULT NULL,
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_ext1_{0……9}` (
  `uid` int(11) NOT NULL,
  `rip` varchar(50) NOT NULL,
  `rtime` datetime NOT NULL,
  `rapp` varchar(20) NOT NULL,
  `rname` varchar(50) NOT NULL,
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_ext2_{0……9}` (
  `uid` int(11) NOT NULL,
  `lltime` datetime NOT NULL,
  `llip` varchar(50) NOT NULL,
  `llapp` varchar(20) NOT NULL,
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_sec_{0……9}` (
  `id` bigint(20) NOT NULL,
  `ask` int(11) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_third_{0……9}` (
  `id` bigint(20) NOT NULL,
  `uid` int(11) NOT NULL,
  `apptype` tinyint(4) DEFAULT NULL,
  `appid` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `credential` varchar(200) DEFAULT NULL,
  `utype` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;